#### Learn-by-doing: ReactJS social card

![Social Card](https://raw.githubusercontent.com/gorbatova/social-card/master/src/assets/images/shot.png "Social Card")

#### Project References
1. I wanted to try ReactJS but didn't have any idea what to build with it. 
So I took an idea about creating a social card form [React Practice Projects](https://daveceddia.com/react-practice-projects/) article. 
Many thanks!
2. I used [
Component Based Design UI Kit](https://dribbble.com/shots/4189201-Component-Based-Design-UI-Kit) from dribble as a design reference.
I really like how this UI kit looks.
3. This [tutorial](https://blog.alexdevero.com/create-quick-simple-react-flipping-card/) helped me on the start.
