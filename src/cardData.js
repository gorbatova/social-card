const cardData = {
	me: {
		name: 'Valya Gorbatova',
		position: 'CSS Engineer',
		location: 'Saint-Petersburg, Russia',
		avatarURL: 'https://raw.githubusercontent.com/gorbatova/social-card/master/src/assets/images/valya.png'
	},
	links: {
		'Github': 'https://github.com/gorbatova',
		'Twitter': 'https://twitter.com/_sonofjesus',
		'Instagram': 'https://instagram.com/_sonofjesus',
		'VK': 'https://vk.com',
		'Last.fm': 'https://www.last.fm/user/sonofjesus'
	},
	footer: {
		text: '2018 valya gorbatova; ',
		src: {
			url: 'https://github.com/gorbatova/social-card',
			text: 'source code on github'
		}
	}
}

export default cardData;