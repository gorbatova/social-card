import React from 'react';

const Footer = props => (
	<footer className="footer">
					<span>
						{props.text}
						<a href={props.srcURL} target="_blank" rel="noopener noreferrer">
							{props.srcTEXT}
						</a>
					</span>
	</footer>
);

export default Footer;