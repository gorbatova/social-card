import React from 'react';
import Content from './Content';
import Header from './Header';

const Card = props => (
	<section className="root">
		<div className="social_card__wrapper">
			<div className="social_card">
				<Header/>
				<Content
					avatarURL={props.avatarURL}
					name={props.name}
					position={props.position}
					location={props.location}
				/>
			</div>
		</div>
	</section>
);

export default Card;