import React, { Component } from 'react';
import SocLinks from './SocItems/SocLinks';
import './../Styles/Dots.css';

class DotControl extends Component {
	constructor(props) {
		super(props)
		this.state = {
			press: false
		}
		this.press = this.press.bind(this)
		this.setWrapperRef = this.setWrapperRef.bind(this);
		this.handleClickOutside = this.handleClickOutside.bind(this)
	}

	componentDidMount() {
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	press() {
		this.setState({
			press: !this.state.press
		})
	}

	setWrapperRef(node) {
		this.wrapperRef = node;
	}

	handleClickOutside(event) {
		if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
			this.setState({
				press: false
			})
		}
	}

	render() {
		return (
			<div ref={this.setWrapperRef}>
				<div className={'dots ' + (this.state.press ? 'links-visible' : 'links-hidden')}>
					<button className={'btn ' + (this.state.press ? 'active' : '')} onClick={this.press}>
						<svg width="24" height="24" viewBox="0 0 24 24" focusable="false" role="presentation"><g fill="currentColor" fillRule="evenodd"><circle cx="5" cy="12" r="2"></circle><circle cx="12" cy="12" r="2"></circle><circle cx="19" cy="12" r="2"></circle></g></svg>
					</button>
					<div className='soc-links'>
						<SocLinks/>
					</div>
				</div>
			</div>
		)
	}
}

export default DotControl;


