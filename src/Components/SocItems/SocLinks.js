import React, { Component } from 'react';
import SocItem from './SocItem';
import data from "../../cardData";

class SocLinks extends Component {
	render() {
		const links = data.links;

		const SocItems = Object.keys(links).map(key =>
			<SocItem
				link={links[key]}
				alt={key}
			/>
		)

		return (
			<div className="social_card__header-links">
				{SocItems}
			</div>
		)
	}
}

export default SocLinks;