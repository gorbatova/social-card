import React from 'react';

const SocItem = props => (
	<div className="social_card__header-link-item">
		<a href={props.link} target="_blank" rel="noopener noreferrer">
			{props.alt}
		</a>
	</div>
)

export default SocItem;