import React, { Component } from 'react';
import Card from './Card';
import Footer from './Footer';
import Switch from './Switch';

import './../Styles/Keyframes.css';
import './../Styles/Switch.css';
import './../Styles/Card.css';

import data from './../cardData';

class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			condition: false
		}
		this.handleClick = this.handleClick.bind(this)
	}

	handleClick() {
		this.setState({
			condition: !this.state.condition
		})
	}

	render() {
		return (
			<main className={ this.state.condition ? "small-view" : "big-view" }>
				<header className="header">
					<Switch
						click={this.handleClick}
					/>
				</header>
				<Card
					avatarURL={data.me.avatarURL}
					name={data.me.name}
					position={data.me.position}
					location={data.me.location}
				/>
				<Footer
					text={data.footer.text}
					srcURL={data.footer.src.url}
					srcTEXT={data.footer.src.text}
				/>
			</main>
		)
	}
}




export default App;