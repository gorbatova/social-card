import React from 'react';
import DotControl from './Dots';

const Content = props => (
	<div className="social_card__content-body">
		<div className="social_card__content">
			<img src={props.avatarURL} className="social_card__avatar" alt="logo" />
			<div className="social_card__content-text">
				<h1 className="social_card__content-title">
					{props.name}
				</h1>
				<p className="social_card__content-descr">
					{props.position}
				</p>
				<p className="social_card__content-descr">
					{props.location}
				</p>
			</div>
			<DotControl/>
		</div>
	</div>
)

export default Content;