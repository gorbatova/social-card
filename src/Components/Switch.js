import React from 'react';

const Switch = props => (
	<div className="view-switcher__wrapp" onClick={props.click}>
		<span className="view-switcher"></span>
	</div>
);

export default Switch;