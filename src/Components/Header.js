import React, { Component } from 'react';
import SocLinks from './SocItems/SocLinks';

class Header extends Component {
	render() {
		return (
			<div className="social_card__header">
				<SocLinks/>
			</div>
		)
	}
}

export default Header;